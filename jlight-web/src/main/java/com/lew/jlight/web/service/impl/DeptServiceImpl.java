package com.lew.jlight.web.service.impl;

import com.google.common.base.Strings;

import com.lew.jlight.mybatis.ParamFilter;
import com.lew.jlight.web.dao.DeptDao;
import com.lew.jlight.web.entity.Dept;
import com.lew.jlight.web.entity.pojo.JSTree;
import com.lew.jlight.web.service.DeptService;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class DeptServiceImpl implements DeptService {

    @Resource
    private DeptDao deptDao;

    @Override
    public void add(Dept dept) {
        checkNotNull(dept,"字典信息不能为空");
        checkArgument(!Strings.isNullOrEmpty(dept.getName()),"字典名称不能为空");
        if (dept.getParentId() == null) {
            dept.setParentId("#");
        }
        deptDao.save(dept);
    }

    @Override
    public List<Dept> getList(ParamFilter queryFilter) {
        return deptDao.find("getList", queryFilter.getParam(), queryFilter.getPage());
    }

    @Override
    public void update(Dept dept) {
        deptDao.update(dept);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public List<JSTree> getTree() {
        return deptDao.findMap("getTree");
    }

    @Override
    public List<Dept> getListByParentId(String parentId) {
        List<Dept> dictList = new LinkedList<>();
        List<Dept> subList = deptDao.find("getListByParentId",parentId);
        Dept dict = deptDao.findUnique("getById",parentId);
        dictList.add(dict);
        if(subList!=null && subList.size()>0){
            dictList.addAll(subList);
        }
        return dictList;
    }

    @Override
    public List<Dept> getCatagory() {
        return deptDao.find("getCatagory");
    }

    @Override
    public void delete(List<String> ids) {
        for (String id : ids) {
            deptDao.delete("delete", id);
        }
    }

	@Override
	public Dept getById(String id) {
		return deptDao.findUnique("getById",id);
	}


}
