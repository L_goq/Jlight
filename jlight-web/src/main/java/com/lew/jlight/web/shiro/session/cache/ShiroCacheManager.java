package com.lew.jlight.web.shiro.session.cache;

import com.lew.jlight.core.page.Page;
import com.lew.jlight.redis.RedisLite;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.util.Destroyable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.Resource;

public class ShiroCacheManager implements CacheManager, Destroyable {
    private static final Logger logger = LoggerFactory.getLogger(ShiroCacheManager.class);

    private final ConcurrentMap<String, Cache> localCache = new ConcurrentHashMap<>();

    @Resource
    private RedisLite redisLite;

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        logger.debug("get instance of RedisCache,name: " + name);
        Cache c = localCache.get(name);
        if (c == null) {
            c = new RedisShiroCache<K,V>(name,redisLite);
            localCache.putIfAbsent(name, c);
            c = localCache.get(name);
        }
        return c;
    }

    @Override
    public void destroy() throws Exception {
        redisLite.flushDB();
    }
}
