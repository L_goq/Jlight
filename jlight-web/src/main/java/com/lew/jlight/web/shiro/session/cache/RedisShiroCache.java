package com.lew.jlight.web.shiro.session.cache;

import com.lew.jlight.redis.RedisLite;
import com.lew.jlight.web.shiro.util.SerializeUtil;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RedisShiroCache<K, V> implements Cache<K, V> {
    private final String REDIS_SHIRO_CACHE = "shiro-cache:";

    private RedisLite redisLite;

    private String name;

    public RedisShiroCache(String name,RedisLite redisLite) {
        this.redisLite = redisLite;
        this.name = name;
    }

    @Override
    public V get(K key) throws CacheException {
        try {
            if (key == null) {
                return null;
            } else {
                byte[] rawValue = redisLite.getByteArray(getKey(key));
                @SuppressWarnings("unchecked")
                V value = (V) SerializeUtil.deserialize(rawValue);
                return value;
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public V put(K key, V value) throws CacheException {
        try {
            redisLite.setByteArray(getKey(key), SerializeUtil.serialize(value));
            return value;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public V remove(K key) throws CacheException {
        try {
            V previous = get(key);
            redisLite.deleteByKey(getKey(key));
            return previous;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public void clear() throws CacheException {
        try {
            String preKey = this.REDIS_SHIRO_CACHE + "*";
            redisLite.deleteByKey(preKey);
            redisLite.flushDB();
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public int size() {
        if (keys() == null)
            return 0;
        return keys().size();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<K> keys() {
        try {
            Set<String> keys = redisLite.keys(this.REDIS_SHIRO_CACHE + "*");
            if (CollectionUtils.isEmpty(keys)) {
                return Collections.emptySet();
            } else {
                Set<K> newKeys = new HashSet<>();
                for (String key : keys) {
                    newKeys.add((K) key);
                }
                return newKeys;
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public Collection<V> values() {
        try {
            Set<String> keys = redisLite.keys(this.REDIS_SHIRO_CACHE + "*");
            if (!CollectionUtils.isEmpty(keys)) {
                List<V> values = new ArrayList<>(keys.size());
                for (String key : keys) {
                    @SuppressWarnings("unchecked")
                    V value = get((K) key);
                    if (value != null) {
                        values.add(value);
                    }
                }
                return Collections.unmodifiableList(values);
            } else {
                return Collections.emptyList();
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    /**
     * 自定义relm中的授权/认证的类名加上授权/认证英文名字
     */
    public String getName() {
        if (name == null)
            return "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获得byte[]型的key
     */
    private String getKey(K key) {
        if (key instanceof String) {
            return this.REDIS_SHIRO_CACHE + getName() + ":" + key;
        } else {
            return String.valueOf(key);
        }
    }
}
